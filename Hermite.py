# -*- coding: utf-8 -*-
import operator

__author__ = 'vojtek.nowak@gmail.com & mateuszdargacz@gmail.com'

import sys
import random
import numpy as np
import matplotlib.pyplot as plt

sys.setrecursionlimit(5000000)


# works for functions with hashable (immuatble) arguments
# Example usage: fib = memoize(fib)
def memoize(f):
    # define "wrapper" function that checks cache for
    # previously computed answer, only calling f if this
    # is a new problem.
    def memf(*x):
        if x not in memf.cache:
            memf.cache[x] = f(*x)
        return memf.cache[x]

    # initialize wrapper function's cache.  store cache as
    # attribute of function so we can look at its value.
    memf.cache = {}
    return memf

class Settings:
    xList = []
    xFrom = -10
    xTo = 10
    maxDitems = 4
    dFrom = -100
    dTo = 100

class Node:
    x = None
    d = None

    def __init__(self,x=None,d=None):

        if x != None and d != None:
            self.x = x
            self.d = d
        else:
            self.genrateX()
            self.generateD()

    def genrateX(self):
        self.x = random.randrange(Settings.xFrom, Settings.xTo)
        while(self.x in Settings.xList):
            self.x = random.randrange(Settings.xFrom, Settings.xTo)
        Settings.xList.append(self.x)

        return self.x

    def generateD(self):
        self.d = list()
        for i in range(random.randrange(1,Settings.maxDitems+1)):
            x = random.randrange(Settings.dFrom, Settings.dTo)
            self.d.append(x)

        return self.d

    def __repr__(self):
        return "X: "+ str(self.x) + "\nPochodne: "+ " | ".join(str(s) for s in self.d)

class Hermite:
    nodes = None
    howManyNodes = None
    m = None
    tableT = None
    biS = []


    def __init__(self):
        self.getData()

        self.nodes = list()
        self.appendNodes()

        for node in self.nodes:
            print node

        self.calcM()

        self.calcTableT()

        print self.tableT

        #MEMOIZATION OF RECURSIVE CALCULATIONS
        self.calcSubset = memoize(self.calcSubset)
        self.silnia = memoize(self.silnia)


        #DRUKOWANIE WSPÓŁCZYNIKÓW Bi
        print str("*"*21)
        print "*\tWspółczynniki b\t*"
        print str("*"*21)
        for i in range(self.m):
            if i==0:
                print "B"+str(self.m-i)+" = "+str(self.calcSubset(self.tableT))
            else:
                print "B"+str(self.m-i)+" = "+str(self.calcSubset(self.tableT[:-i]))
        print "B0 = "+str(self.nodes[0].d[0])
        self.appendBiS()
        self.showPlot()

    def getData(self):
        self.howManyNodes = int(raw_input("Podaj ilość wezłów:"))

    def appendBiS(self):
        for i in range(self.m):
            if i==0:
                self.biS.insert(0,self.calcSubset(self.tableT))
            else:
                self.biS.insert(0,self.calcSubset(self.tableT[:-i]))
        self.biS.insert(0,self.nodes[0].d[0])

    def appendNodes(self):
        if self.howManyNodes != 0:
            for i in range(self.howManyNodes):
                self.nodes.append(Node())
            self.nodes.sort(key=operator.attrgetter('x'))
        elif self.howManyNodes == 0:
        #TESTOWE DANE
            self.nodes.append(Node(0,[0,1]))
            self.nodes.append(Node(1,[1,0,2]))
            self.nodes.append(Node(2,[2]))

        #else:
        #    self.nodes.append(Node(1,[3,2]))
        #    self.nodes.append(Node(3,[5,6]))

            #self.nodes.append(Node(-1,[2,-8,56]))
            #self.nodes.append(Node(0,[1,0,0]))
            #self.nodes.append(Node(1,[2,8,56]))

            #self.nodes.append(Node(0,[0]))
            #self.nodes.append(Node(1,[1]))
            #self.nodes.append(Node(2,[2]))


    def calcM(self):
        suma = 0
        for node in self.nodes:
            suma+=len(node.d)
        self.m = suma-1


    def calcTableT(self):
        self.tableT = list()
        for p in self.nodes:
            for i in range(len(p.d)):
                self.tableT.append(p.x)
        #must be unmutable
        self.tableT = tuple(sorted(self.tableT))


    def silnia(self,x):
        if x == 0:
            return 1
        else:
            return x * self.silnia(x-1)

    def calcDerivetive(self,k,tj):
        for node in self.nodes:
            if node.x == tj:
                return node.d[k]/self.silnia(k)*1.0


    def calcNewton(self,arg1,arg2):
        return ((self.calcSubset(arg1) - self.calcSubset(arg2))*1.0)/(arg1[-1] - arg2[0])


    def calcSubset(self, arguments):
        #Pojedycncza wartosc
        if len(arguments)==1:
            for node in self.nodes:
                if node.x == arguments[0]:
                    return node.d[0]
        #Wszystkie takie same
        elif all(arg == arguments[0] for arg in arguments):
            return self.calcDerivetive(len(arguments)-1,arguments[0])
        #Różne wartości
        else:
            return self.calcNewton(arguments[1:],arguments[:-1])

    def genEquation(self,x):
        eq = 0
        for m in xrange(self.m+1):
            if m == 0:
                eq += self.biS[0]
            else:
                eq_s = x-self.tableT[0]
                for j in xrange(1, m):
                    eq_s *= (x-self.tableT[j])
                eq += self.biS[m]*eq_s
        return eq

    def showPlot(self):
        fig = plt.figure()

        #Wykres wygenerowanych pkt f(Xi)
        pointPlot = fig.add_subplot(111)


        x = [node.x for node in sorted(self.nodes, key=operator.attrgetter('x'))]
        y = [node.d[0] for node in sorted(self.nodes, key=operator.attrgetter('x'))]
        pointPlot.plot(x, y, 'ro')
        pointPlot.axis([x[0]-3, x[-1]+3, y[0]-3, y[-1]+3])

        pointPlot.set_xlabel(u'Os X')
        pointPlot.set_ylabel(u'Os Y')
        pointPlot.set_title('Hermite')

        #Wykres b 0 + b 1 (x) + b 2 (x − t 0 ) + b 3 (x − t 0 )...
        hermitePlot = fig.add_subplot(111)

        h_x = np.arange(x[0]-3, x[-1]+3, 0.01)
        h_y = map(self.genEquation, h_x)

        hermitePlot.plot(h_x, h_y, 'b-')

        plt.show()


if __name__ == "__main__":
    h = Hermite()
